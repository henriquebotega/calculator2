import React from 'react'
import { TouchableOpacity, StyleSheet, Text, Dimensions } from 'react-native'

const screen = Dimensions.get('window')
const buttonWidth = screen.width / 4

const styles = StyleSheet.create({
    text: {
        color: '#fff',
        fontSize: 25,
    },
    button: {
        backgroundColor: '#333',
        flex: 1,
        height: Math.floor(buttonWidth - 10),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: Math.floor(buttonWidth),
        margin: 5
    },
    buttonDouble: {
        flex: 0,
        width: screen.width / 2 - 10,
        // alignItems: 'flex-start',
        // padding: 40,
    },
    secondary: {
        backgroundColor: '#a6a6a6'
    },
    accent: {
        backgroundColor: '#f09a36'
    },
    textSecondary: {
        color: '#060606'
    }
})

export default ({ onPress, text, size, theme }) => {
    const buttonStyles = [styles.button]
    const textStyles = [styles.text]

    if (size === 'double') {
        buttonStyles.push(styles.buttonDouble)
    }

    if (theme === 'secondary') {
        buttonStyles.push(styles.secondary)
        textStyles.push(styles.textSecondary)
    }

    if (theme === 'accent') {
        buttonStyles.push(styles.accent)
    }

    return (
        <TouchableOpacity onPress={onPress} style={buttonStyles}>
            <Text style={textStyles}>{text}</Text>
        </TouchableOpacity>
    )
}